#include <stdio.h>

typedef struct{
  long nim;
  char nama[25];
  float ip;
} datamhs;

datamhs RekMhs, RekMhsUpdate;
FILE *MhsMaster, *MhsTemp;
int nimUpd,nimPilih,nimCurr,nimExist;

int isNimExist(long nim){
	FILE *fMaster;
	int c = 0;
	fMaster = fopen("mhs3.dat", "rb");
	while (!feof(fMaster))
	{
		fread(&RekMhs, sizeof(RekMhs), 1, fMaster);
			
		if (nim == RekMhs.nim)
		{
			fclose(fMaster);
			return 1;
		}
	}
	fclose(fMaster);
	return 0;
}

main(){
	
	printf("Masukan NIM mahasiswa yg akan diupdate : ");scanf("%ld",&nimUpd);
	nimExist = isNimExist(nimUpd);
	if(nimExist == 0){
		printf("Nim %d tidak terdapat pada file", nimUpd);
	}else{
		MhsMaster = fopen("mhs3.dat", "rb");
		MhsTemp = fopen("mhsTemp.dat", "wb");
		
		while (fread(&RekMhs, sizeof(RekMhs), 1, MhsMaster))
	  	{
	  		nimCurr = RekMhs.nim;
	  		if(nimUpd != nimCurr){
	  			fwrite(&RekMhs, sizeof(RekMhs), 1, MhsTemp);
			}else{
				printf("Masukan IP yg akan di-update : ");scanf("%f",& RekMhs.ip);
				fwrite(&RekMhs, sizeof(RekMhs), 1, MhsTemp);
			}
	  	}
	  	fclose(MhsMaster);
	  	fclose(MhsTemp);
	  	
	  	MhsMaster = fopen("mhs3.dat", "wb");
		MhsTemp = fopen("mhsTemp.dat", "rb");
		while (fread(&RekMhs, sizeof(RekMhs), 1, MhsTemp)){
			fwrite(&RekMhs, sizeof(RekMhs), 1, MhsMaster);
		}
		fclose(MhsMaster);
		fclose(MhsTemp);
		printf("Data berhasil di-update");
	}
	
}
