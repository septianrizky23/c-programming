#include <stdio.h>

typedef struct{
  long nim;
  char nama[25];
  float ip;
} datamhs;
datamhs RekMhs1, RekMhs2;
FILE *Mhs1, *Mhs2, *Mhs4;

main(){
  Mhs1=fopen("mhs1.dat", "rb");
  Mhs2=fopen("mhs2.dat", "rb");
  Mhs4=fopen("mhs4.dat", "wb");
  
  fread(&RekMhs1, sizeof(RekMhs1), 1, Mhs1);
  fread(&RekMhs2, sizeof(RekMhs2), 1, Mhs2);

  	while(!feof(Mhs2) || !feof(Mhs1))
	{
	    if (feof(Mhs1)){
	      fwrite(&RekMhs2, sizeof(RekMhs2), 1, Mhs4);
	      fread(&RekMhs2, sizeof(RekMhs2), 1, Mhs2);
	    }else if (feof(Mhs2)){
	      fwrite(&RekMhs1, sizeof(RekMhs1), 1, Mhs4);
	      fread(&RekMhs1, sizeof(RekMhs1), 1, Mhs1);
	    }else if(RekMhs1.nim <= RekMhs2.nim){
	      fwrite(&RekMhs1, sizeof(RekMhs1), 1, Mhs4);
	      fread(&RekMhs1, sizeof(RekMhs1), 1, Mhs1);
	    }
	    else
	    {
	      fwrite(&RekMhs2, sizeof(RekMhs2), 1, Mhs4);
	      fread(&RekMhs2, sizeof(RekMhs2), 1, Mhs2);
	    }
  	}

	fclose(Mhs1);
	fclose(Mhs2);
	fclose(Mhs4);
}

