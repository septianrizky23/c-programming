#include<stdio.h>
#include<string.h>
#include<conio.h>

typedef struct mahasiswa{
	int nim;			
	char nama[100];	
	float ipk;		
	mahasiswa *next;
}nodeMahasiswa;

nodeMahasiswa *head,*baru,*bantu,*bantu2;

nodeMahasiswa* getNewNode(int nim,char nama[100],float ipk){
	baru = new nodeMahasiswa;
	baru->nim = nim;
	baru->ipk = ipk;
	strcpy(baru->nama, nama);
	return baru;
}

void inisialisasi(){
	head = new nodeMahasiswa ;
	head = NULL;
}

void addDepan(int NIM, char NAMA[100], float IPK){
	baru = getNewNode(NIM,NAMA,IPK);
	if(head == NULL){
		head=baru;
		head->next = NULL;
	}else{
		baru->next = head;
		head = baru;
	}
}

void addBelakang(int NIM, char NAMA[100], float IPK){
	baru = getNewNode(NIM,NAMA,IPK);
	if(head == NULL){
		head=baru;		
		head->next = NULL;  	
	}else{
		bantu = head;
		while(bantu->next != NULL){
			bantu = bantu->next;
		}
		bantu->next = baru;
		baru->next = NULL;	
	}
}

void addTengah(int NIM, char NAMA[100], float IPK){
	baru = getNewNode(NIM,NAMA,IPK);
	if(head == NULL){
		head=baru;		
		head->next = NULL;  
	}else{
		if(baru->ipk <= head->ipk){ 
			baru->next = head;
			head = baru;
		}else{
			bantu = head;		
		 	while(baru->ipk > bantu->ipk){  
				if(bantu->next == NULL){
					bantu->next = baru;	
					baru->next = NULL;	
				}else if(baru->ipk > bantu->ipk && baru->ipk < bantu->next->ipk){
					bantu2 = bantu->next; 
					bantu->next = baru;	 
					baru->next = bantu2;	 
				}
		 	bantu = bantu->next; 			
			}
		}
	}
}

void hapusDepan(){
  nodeMahasiswa*hapus;			
  if(head == NULL){			
     printf("Data Kosong\n");
  }else{
     if(head->next == NULL){		
        head = NULL;
     }else{
       hapus = head;		
       head = head->next;
       hapus = NULL;
     }
     printf("Data berhasil dihapus\n");	
  }
}

void hapusBelakang(){
   nodeMahasiswa*hapus, *temp;
   if(head == NULL){
      printf("Data Kosong\n");
   }else{
      if(head->next == NULL){
         head = NULL;
      }else{
         hapus = head;			
         while(hapus->next != NULL){
            temp = hapus;
            hapus = hapus->next;
         }
         temp->next = NULL;
         hapus = NULL;	
      }
      printf("Data berhasil dihapus\n");	
    }
}

void HapusNIM(int cari){
   nodeMahasiswa*hapus, *temp, *bantu;
   bool found = false;
   if(head == NULL){
      printf("Data Kosong\n");
   }else{
      hapus = head;
      if(hapus->nim == cari){		
         if(hapus->next == NULL){	
           head = NULL;
         }else{			
           hapus = head;
           head = head->next;
           hapus = NULL;
         }
      }else{
        while(hapus->next != NULL){			
               if(hapus->nim == cari){		
                   found = true;
                   break;
               }
               temp = hapus;
               hapus = hapus->next;
            }            
            if(found){
                if(hapus->next == NULL){		
                    temp->next = NULL;
                    hapus = NULL;
                }else{
                    bantu = hapus->next;		
                    temp->next = bantu;
                    hapus = NULL;
                }
            }else{
            printf("Data tidak ditemukan\n");
         }
      }
   }
}


void searchTidakTerurut(int cari){
	if(head == NULL){
		printf("List Masih Kosong\n");
	}else{
		bantu = head;
		while(bantu->next != NULL && bantu->nim != cari){
			bantu = bantu->next;
		}
		if(bantu->nim == cari){
			printf("Data Ditemukan\n");
		}else{
			printf("Data Tidak Ditemukan\n");
		}
	}
}

void searchTerurut(int cari){
	if(head == NULL){
		printf("List Masih Kosong\n");
	}else{
		bantu = head;
		while(bantu->next != NULL && bantu->nim < cari){
			bantu = bantu->next;
		}
		if(bantu->nim == cari){
			printf("Data Ditemukan\n");
		}else{
			printf("Data Tidak Ditemukan\n");
		}
	}
}

void tampilData(){
	if(head == NULL){
		printf("List Masih Kosong\n");
	}else{
		bantu = head;
		while(bantu != NULL){
			printf("%d\t%s\t%0.2f\n", bantu->nim, bantu->nama, bantu->ipk);
			bantu = bantu->next;
		}
	}
}


main(){
	int pilih;
	int nim;			
	char nama[100];	
	float ipk;
	
	inisialisasi();
	do{
		printf("------------MENU INPUT DATA MAHASISWA------------\n");
		printf("1. Add Data di Depan \n");
		printf("2. Add Data di Belakang \n");
		printf("3. Add Data di Tengah \n");
		printf("4. Hapus Depan \n");
		printf("5. Hapus Belakang \n");
		printf("6. Hapus Tengah \n");
		printf("7. Tampilkan Data \n");
		printf("8. Pencarian Tidak Terurut \n");
		printf("9. Pencarian Terurut \n");
		printf("0. Keluar \n");
		printf("Masukan Pilihan: ");scanf("%d",& pilih);
		
		switch(pilih){
			
			case 1: printf("Masukan NIM : ");scanf("%d",& nim);
					printf("Masukan Nama : ");scanf("%s",& nama);
					printf("Masukan IPK : ");scanf("%f",& ipk);
					addDepan(nim,nama,ipk);
					break;
			case 2: printf("Masukan NIM : ");scanf("%d",& nim);
					printf("Masukan Nama : ");scanf("%s",& nama);
					printf("Masukan IPK : ");scanf("%f",& ipk);
					addBelakang(nim,nama,ipk);
					break;
			case 3: printf("Masukan NIM : ");scanf("%d",& nim);
					printf("Masukan Nama : ");scanf("%s",& nama);
					printf("Masukan IPK : ");scanf("%f",& ipk);
					addTengah(nim,nama,ipk);
					break;
			case 4: hapusDepan();break;
			case 5: hapusBelakang();break;
			case 6: printf("Masukan NIM : ");scanf("%d",& nim);
					HapusNIM(nim);
					break;
			case 7: tampilData();break;
			case 8: printf("Masukan NIM : ");scanf("%d",& nim);
					searchTidakTerurut(nim);
					break;
			case 9: printf("Masukan NIM : ");scanf("%d",& nim);
					searchTerurut(nim);
					break;
			case 0: printf("Anda telah keluar dari program\n");
		}
	
		
		
	}while(pilih != 0);
	
	
}
